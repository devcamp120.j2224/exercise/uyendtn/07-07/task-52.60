import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

public class Order2 {
    int id; // id của order
    String customerName; // tên khách hàng
    Integer price;// tổng giá tiền
    Date orderDate; // ngày thực hiện order
    Boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua
    Person buyer;// người mua, là một object thuộc class Person

   

    public Order2() {
        
        
    }
    public Order2(int id) {
        this.id = id;
    }
    public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm, String[] items,
            Person buyer) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
        this.buyer = buyer;
    }
    public Order2(int id, String customerName) {
        this.id = id;
        this.customerName = customerName;
    }
    @Override
        public String toString() {
        return "Order = [customerName: " + customerName + " id: " + id + " order date: " + orderDate 
                        + " confirm: " + confirm + " items: "
                        + items[0] +", " + items[1]+",], buyer " + buyer + "]" ;
}
}
