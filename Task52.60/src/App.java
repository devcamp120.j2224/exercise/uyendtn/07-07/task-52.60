import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        //tạo object ngày đơn hàng
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = format.parse("2022-07-15");
        DateFormat dayFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println(dayFormat.format(date1));

        //tạo đối tương buyer
        Person buyer = new Person("OHMM");
        //tạo array item
        String[] items = {"chair", "table"};

        //tạo và format giá
        Integer price = 500000;
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        System.out.println(formatter.format(price)+" VND"); 

        //tạo 4 đối tượng Order2
        Order2 newOrder2 = new Order2();
        newOrder2.orderDate = date1;
        newOrder2.id = 1;
        newOrder2.customerName = "OHMM";
        newOrder2.price = 500000;
        newOrder2.buyer = buyer;
        newOrder2.items = items;

        Order2 newOrder1 = new Order2(2);
        newOrder1.items = items;
        Order2 newOrder3 = new Order2(3, "Xtra");
        newOrder3.items = items;
        Order2 newOrder4 = new Order2(4, "Universal", 500000, date1, true, items, buyer);

        //tạo list và add 4 đối tượng vào
        ArrayList<Order2> orderList = new ArrayList<Order2>();
        orderList.add(newOrder1);
        orderList.add(newOrder2);
        orderList.add(newOrder3);
        orderList.add(newOrder4);

        System.out.println(orderList);
        orderList.toString();
        
    }


    
}
